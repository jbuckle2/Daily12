// respond to button click
console.log("Page load happened!")

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from first name, last name and email
    var first_text = document.getElementById('input-text-firstname').value;
    var last_text = document.getElementById('input-text-lastname').value;
    var email_text = document.getElementById('input-email').value;
    console.log('title:' + first_text + ' author: ' + last_text + ' story ' + email_text);

    // get checkbox state
    var shirts_string = "";
    if (document.getElementById('checkbox-blue').checked){
        console.log('detected blue!');
        shirts_string += "Blue,";
    }
    if (document.getElementById('checkbox-red').checked){
        console.log('detected red!');
        shirts_string += "Red,";
    }
    if (document.getElementById('checkbox-yellow').checked){
        console.log('detected yellow!');
        shirts_string += "Yellow,";
    }
    if (document.getElementById('checkbox-black').checked){
        console.log('detected black!');
        shirts_string += "Black";
    }

    // make shirts combined string
    console.log('Shirt colors selected: ' + shirts_string);

    //select dropdown select-server-address
    var selindex = document.getElementById('select-size').selectedIndex;
    var shirt_size = document.getElementById('select-size').options[selindex].value;

    // make dictionary
    shirts_dict = {};
    shirts_dict['first'] = first_text;
    shirts_dict['last'] = last_text;
    shirts_dict['email'] = email_text;
    shirts_dict['shirts'] = shirts_string;
    shirts_dict['size'] = shirt_size;
    console.log(shirts_dict);

    displayOrder(shirts_dict);
}

function displayOrder(shirts_dict){
    console.log('entered displayOrder!');
    console.log(shirts_dict);
    // get fields from order and display in label.
    var order_top = document.getElementById('order-top-line');
    order_top.innerHTML = shirts_dict['first'] + ' ' + shirts_dict['last'];

    var order_body = document.getElementById('order-body');
    order_body.innerHTML = shirts_dict['shirts'] + '    Size: ' + shirts_dict['size']; 
}
